const {
  GraphQLServer
} = require("graphql-yoga");
const {
  createWriteStream
} = require("fs");

const typeDefs = `
  scalar Upload

  type Mutation {
    uploadFile(file: [Upload!]!): Boolean
  }

  type Query {
    hello: String
  }
`;

const storeUpload = ({
    stream,
    mimetype
  }) =>
  new Promise((resolve, reject) => {

    var custom_filename = new Date().getTime();

    stream
      .pipe(createWriteStream(`./public/${custom_filename}.${mimetype.split('/')[1]}`))
      .on("finish", () => resolve())
      .on("error", reject)

  });

const resolvers = {
  Mutation: {
    uploadFile: async (parent, {
      file
    }) => {


      for (let i = 0; i < file.length; i++) {

        const {
          stream,
          mimetype
        } = await file[i];
        await storeUpload({
          stream,
          mimetype
        });

      }
      return true;
    }
  },
  Query: {
    hello: () => "hi"
  }
};

const server = new GraphQLServer({
  typeDefs,
  resolvers
});
server.start(() => console.log("Server is running on localhost:4000"));