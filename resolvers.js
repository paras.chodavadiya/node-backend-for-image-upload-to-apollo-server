var fs = require('file-system');

module.exports = {
  Query: {
    hello: () => "hi"
  },
  Mutation: {
    uploadFile: (parent, {
      file
    }) => {


      for (let i = 0; i < file.length; i++) {
        var filename = new Date().getTime();

        // console.log(`${filename}.${file[i].type.split('/')[1]}`);

        var base64Data = file[i].data.replace(/^data:image\/png;base64,/, "");


        var buf = new Buffer(base64Data, 'base64');


        fs.writeFile(`./public/${filename}.${file[i].type.split('/')[1]}`,
          buf,
          // 'base64',
          function (err) {
            // console.log(err);
          })
      }
      return true;
    }
  }
};