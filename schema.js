module.exports = `

input Upload {
  name: String!
  type: String!
  uri: String!
  data: String!
}

type Mutation {
  uploadFile(file: [Upload!]!): Boolean!
}

type Query {
  hello: String
}

`;