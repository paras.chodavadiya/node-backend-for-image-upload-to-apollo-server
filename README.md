# Node Backend for Image Upload To Apollo Server

A code snippet for uploading pictures to Apollo Server


# Apollo Server with Node

This is backend code with Node and Apollo Server integration for ReactNative App


## Installation

```bash
npm install
```

## Start the node server

```bash
node index.js
```
